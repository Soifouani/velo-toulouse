const table = document.getElementById('table');
const btnShowTable = document.getElementById('btn-show-table');
const close = document.getElementById('close');

btnShowTable.addEventListener('click', showParkInfo);
close.addEventListener('click', closeParkInfo);

function showParkInfo() {
    table.style.display = 'block';
}

function closeParkInfo() {
    table.style.display ='none';
}